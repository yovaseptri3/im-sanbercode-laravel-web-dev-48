@extends('layouts.master')

@section('judul')
Pendaftaran
@endsection
@section('content')

    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="post">
        @csrf
        <label>First name:</label> <br><br>
        <input type="text" name = fname> <br><br>
        <label>Last name:</label> <br><br>
        <input type="text" name = lname> <br><br>
        <label>Gender:</label> <br><br>
        <input type="radio" name="Gender"> Male <br>
        <input type="radio" name="Gender"> Female <br>
        <input type="radio" name="Gender"> Other <br><br>
        <label>Nationality:</label> <br><br>
        <select name="nationality">
            <option value="">Indonesian</option>
            <option value="">Singaporean</option>
            <option value="">Malaysian</option>
            <option value="">Australian</option>
        </select><br><br>
        <label>Languange Spoken:</label> <br><br>
        <input type="checkbox" name="languange spoken"> Bahasa Indonesia <br>
        <input type="checkbox" name="languange spoken"> English <br>
        <input type="checkbox" name="languange spoken"> Other <br><br>
        <label>Bio:</label> <br><br>
        <textarea  cols="30" rows="10"></textarea><br><br>


        <input type="submit" value="Sign Up">
    </form>
</body>
</html>

@endsection