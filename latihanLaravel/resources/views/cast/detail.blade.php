@extends('layouts.master')

@section('judul')
    Detail Cast
@endsection

@section('content')

    <h1 class="text-primary">{{$castData->nama}}</h1>
    <p>{{$castData->bio}}</p>
    
@endsection