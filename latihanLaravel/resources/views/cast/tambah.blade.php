@extends('layouts.master')

@section('judul')
    Tambah Cast
@endsection

@section('content')
<form method="post" action="/cast">
    @csrf
    
    <div class="form-group">
      <label>Nama Cast</label>
      <input type="text" name="nama" class="form-control @error('nama') is-invalid @enderror" placeholder="Masukkan Nama">
    </div>

    @error('nama')
      <div class="alert alert-danger"> {{$message}} </div>
    @enderror
      
    <div class="form-group">
        <label>Umur Cast</label>
        <input type="number" name="umur" class="form-control @error('umur') is-invalid @enderror" placeholder="Masukkan Umur" step="1" min=1>
    </div>

    @error('umur')
      <div class="alert alert-danger"> {{$message}} </div>
    @enderror

    <div class="form-group">
      <label>Bio Cast</label>
      <textarea name="bio" id="" cols="30" rows="10" class="form-control @error('bio') is-invalid @enderror"></textarea>    
    </div>

    @error('bio')
      <div class="alert alert-danger"> {{$message}} </div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection