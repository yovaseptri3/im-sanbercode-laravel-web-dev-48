<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home']);
Route::get('/register', [AuthController::class, 'register']);
Route::post('/welcome', [AuthController::class, 'welcome']);
Route::get('/table', function(){
    return view('page.table');
});
Route::get('/data-table', function(){
    return view('page.data-table');
});

//CRUD cast
//C => Create Data
//Route yang mengarah ke halaman tambah cast
Route::get('/cast/create ', [CastController::class, 'create']);
//Route yang menyimpan data inputan masuk ke database & validasi
Route::post('/cast', [CastController::class, 'store']);

//R => Read Data
//Route yang menampilkan semua data di database ke blade/web browsernya
Route::get('/cast', [CastController::class, 'index']);
//Route yang menampilkan detail data berdasarkan id
Route::get('/cast/{id}', [CastController::class, 'show']);

//U => Update Data
//Route yang mengarah kehalaman form update data berdasarkan id
Route::get('/cast/{id}/edit', [CastController::class, 'edit']);
//Route yang mengupdate data ke database berdasarkan id
Route::put('/cast/{id}', [CastController::class, 'update']);

//D => Delete Data
//Route yang mendelete data berdasarkan id
Route::delete('/cast/{id}', [CastController::class, 'destroy']);
